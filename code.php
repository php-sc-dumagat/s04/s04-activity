<?php

class Building {
    // private access modifier disables direct access to an object's property or methods
    /* protected access modifier allows inheritance of properties and methods to child classes. However, it will still disable direct access to its properties and methods. */
    protected $name;
    protected $floors;
    protected $address;
    
    public function __construct($name, $floors, $address){
        $this->name = $name;
        $this->floors = $floors;
        $this->address = $address;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getFloors() {
        return $this->floors;
    }

    public function setFloors($floors) {
        $this->floors = $floors;
    }

    public function getAddress() {
        return $this->address;
    }

    public function setAddress($floors) {
        $this->address = $address;
    }
}

class Condominium extends Building {
    
}

$building = new Building('Caswynn Building', 8, 'Timog Avenue, Quezon City, Philippines');

$condominium = new Condominium('Enzo Condo', 5, 'Buendia Avenue, Makati City, Philippines');